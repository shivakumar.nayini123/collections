﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            collections col = new collections();
            col.sortedlists();
            col.linkedlists();
            col.queues();
            col.dictionaries();


        }
        public class collections
        {
            public void sortedlists()
            {
                var sorted = new SortedList<string, int>();

                sorted.Add("coins", 3);
                sorted.Add("books", 41);
                sorted.Add("spoons", 5);

                if (sorted.ContainsKey("books"))
                {
                    Console.WriteLine("There are books in the list");
                }

                foreach (var pair in sorted)
                {
                    Console.WriteLine(pair);
                }
            }
            public void linkedlists()
            {
                var nums = new LinkedList<int>();

                nums.AddLast(23);
                nums.AddLast(34);
                nums.AddLast(33);
                nums.AddLast(11);
                nums.AddLast(6);
                nums.AddFirst(9);
                nums.AddFirst(7);

                LinkedListNode<int> node = nums.Find(6);
                nums.AddBefore(node, 5);

                foreach (int num in nums)
                {
                    Console.WriteLine(num);
                }
            }
            public void queues()
            {

                var msgs = new Queue<string>();

                msgs.Enqueue("Message 1");
                msgs.Enqueue("Message 2");
                msgs.Enqueue("Message 3");
                msgs.Enqueue("Message 4");
                msgs.Enqueue("Message 5");

                Console.WriteLine(msgs.Dequeue());
                Console.WriteLine(msgs.Peek());
                Console.WriteLine(msgs.Peek());

                Console.WriteLine();

                foreach (string msg in msgs)
                {
                    Console.WriteLine(msg);
                }
            }
            public void dictionaries()
            {
                var domains = new Dictionary<string, string>();

                domains.Add("de", "Germany");
                domains.Add("sk", "Slovakia");
                domains.Add("us", "United States");
                domains.Add("ru", "Russia");
                domains.Add("hu", "Hungary");
                domains.Add("pl", "Poland");

                Console.WriteLine(domains["sk"]);
                Console.WriteLine(domains["de"]);

                Console.WriteLine("Dictionary has {0} items",
                    domains.Count);

                Console.WriteLine("Keys of the dictionary:");

                var keys = new List<string>(domains.Keys);

                foreach (string key in keys)
                {
                    Console.WriteLine("{0}", key);
                }

                Console.WriteLine("Values of the dictionary:");

                var vals = new List<string>(domains.Values);

                foreach (string val in vals)
                {
                    Console.WriteLine("{0}", val);
                }

                Console.WriteLine("Keys and values of the dictionary:");

                foreach (KeyValuePair<string, string> kvp in domains)
                {
                    Console.WriteLine("Key = {0}, Value = {1}",
                        kvp.Key, kvp.Value);
                }
            }
            
        }
      
    }
}
